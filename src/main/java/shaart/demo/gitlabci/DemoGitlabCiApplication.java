package shaart.demo.gitlabci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoGitlabCiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoGitlabCiApplication.class, args);
    }

}
