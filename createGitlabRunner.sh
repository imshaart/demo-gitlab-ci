mkdir -p /tmp/shared/gitlab-runner

docker run -d --name gitlab-runner \
  --restart unless-stopped \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /tmp/shared/gitlab-runner:/etc/gitlab-runner:z \
  gitlab/gitlab-runner:latest

cat /tmp/shared/gitlab-runner/config.toml