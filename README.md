# demo-gitlab-ci

## Create and register own gitlab runner
Manual: https://habr.com/ru/post/543182/
1. Create gitlab-runner
```shell
./createGitlabRunner.sh
```
2. Register gitlab-runner in GitLab
```shell
./registerGitlabRunner.sh
```
Parameters:
- Instance - your GitLab domain. 
  - Here: `https://gitlab.com`
- Registration token - token from your repo from Settings -> CI/CD -> Runners.<br/>
  - Look at value at https://gitlab.com/imshaart/demo-gitlab-ci/-/settings/ci_cd 
- Description - runner's name.
  - Here: `demo-gitlab-ci-runner`
- Tags - tags for jobs attaching
  - Here: `shaart,smactbus`
- Maintenance note - any note
  - Here: `macbook`
- Executor - what will execute your builds
  - Here: `docker`
- Docker image - image to run your pipelines
  - Here: `3.8.6-openjdk-18`

3. Take a look at `/etc/gitlab-runner/config.toml` inside container
4. Change `volumes = ["/cache"]` to `volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]`
5. Restart runner via `docker restart gitlab-runner`

### Result
![img.png](readme-content/img.png)