FROM openjdk:17.0.2-slim
VOLUME /tmp
COPY target/demo-gitlab-ci-*.jar app.jar
ENV JAVA_OPTS=""
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar /app.jar"]